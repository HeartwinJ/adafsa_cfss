module.exports = {
  content: ["./src/**/*.{html,ts}"],
  theme: {
    extend: {
      colors: {
        header: "#b9b4b4",
        sidebar: "#666666",
        hover: "#808080",
        selected: "#e02222",
      },
    },
  },
  plugins: [],
};
