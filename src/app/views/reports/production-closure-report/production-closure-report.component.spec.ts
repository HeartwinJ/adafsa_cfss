import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductionClosureReportComponent } from './production-closure-report.component';

describe('ProductionClosureReportComponent', () => {
  let component: ProductionClosureReportComponent;
  let fixture: ComponentFixture<ProductionClosureReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductionClosureReportComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProductionClosureReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
