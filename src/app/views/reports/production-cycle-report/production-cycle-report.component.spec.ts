import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductionCycleReportComponent } from './production-cycle-report.component';

describe('ProductionCycleReportComponent', () => {
  let component: ProductionCycleReportComponent;
  let fixture: ComponentFixture<ProductionCycleReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductionCycleReportComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProductionCycleReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
