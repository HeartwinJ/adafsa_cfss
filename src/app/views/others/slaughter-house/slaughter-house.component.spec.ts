import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SlaughterHouseComponent } from './slaughter-house.component';

describe('SlaughterHouseComponent', () => {
  let component: SlaughterHouseComponent;
  let fixture: ComponentFixture<SlaughterHouseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SlaughterHouseComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SlaughterHouseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
