import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { DashboardLayoutComponent } from './layouts/dashboard-layout/dashboard-layout.component';
import { HeaderComponent } from './components/header/header.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { ChevronDownComponent } from './icons/chevron-down/chevron-down.component';
import { ChevronRightComponent } from './icons/chevron-right/chevron-right.component';
import { MenuComponent } from './icons/menu/menu.component';
import { UnderConstructionComponent } from './components/under-construction/under-construction.component';
import { ExclaimationComponent } from './icons/exclaimation/exclaimation.component';
import { SkipNavigationDirective } from './directives/skip-navigation.directive';
import { HomeComponent } from './views/home/home.component';
import { EligibilityUploadComponent } from './views/eligibility-upload/eligibility-upload.component';
import { ApprovalListComponent } from './views/approval-list/approval-list.component';
import { FarmLicenseExpiryComponent } from './views/farm-license-expiry/farm-license-expiry.component';
import { CancelFeedOrderComponent } from './views/cancel-feed-order/cancel-feed-order.component';
import { CancelInvoiceComponent } from './views/cancel-invoice/cancel-invoice.component';
import { GuideForServicesComponent } from './views/guide-for-services/guide-for-services.component';
import { FarmComponent } from './views/farmsMaster/farm/farm.component';
import { EstablishmentTypeComponent } from './views/farmsMaster/establishment-type/establishment-type.component';
import { AreaComponent } from './views/farmsMaster/area/area.component';
import { RegionComponent } from './views/farmsMaster/region/region.component';
import { FeedItemComponent } from './views/feedItemsMaster/feed-item/feed-item.component';
import { FeedItemCategoryComponent } from './views/feedItemsMaster/feed-item-category/feed-item-category.component';
import { ItemBusinessActivityComponent } from './views/feedItemsMaster/item-business-activity/item-business-activity.component';
import { SupplierComponent } from './views/others/supplier/supplier.component';
import { BreedComponent } from './views/others/breed/breed.component';
import { VaccinationProgrammeComponent } from './views/others/vaccination-programme/vaccination-programme.component';
import { InspectionStatusComponent } from './views/others/inspection-status/inspection-status.component';
import { DayOldChickStoreComponent } from './views/others/day-old-chick-store/day-old-chick-store.component';
import { HatchingEggSourceComponent } from './views/others/hatching-egg-source/hatching-egg-source.component';
import { SlaughterHouseComponent } from './views/others/slaughter-house/slaughter-house.component';
import { UserCreationComponent } from './views/settings/user-creation/user-creation.component';
import { ApprovalSettingsComponent } from './views/settings/approval-settings/approval-settings.component';
import { SettingsComponent } from './views/settings/settings/settings.component';
import { FarmsReportComponent } from './views/reports/farms-report/farms-report.component';
import { EligibilityReportComponent } from './views/reports/eligibility-report/eligibility-report.component';
import { SalesReportComponent } from './views/reports/sales-report/sales-report.component';
import { SubsidyPaymentReportComponent } from './views/reports/subsidy-payment-report/subsidy-payment-report.component';
import { SubsidyPaymentSummaryComponent } from './views/reports/subsidy-payment-summary/subsidy-payment-summary.component';
import { InspectionReportComponent } from './views/reports/inspection-report/inspection-report.component';
import { ProductionCycleReportComponent } from './views/reports/production-cycle-report/production-cycle-report.component';
import { FulfilmentReportComponent } from './views/reports/fulfilment-report/fulfilment-report.component';
import { ProductionClosureReportComponent } from './views/reports/production-closure-report/production-closure-report.component';
import { FeedOrderEnquiryComponent } from './views/reports/feed-order-enquiry/feed-order-enquiry.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthLayoutComponent,
    DashboardLayoutComponent,
    HeaderComponent,
    SidebarComponent,
    ChevronDownComponent,
    ChevronRightComponent,
    MenuComponent,
    UnderConstructionComponent,
    ExclaimationComponent,
    SkipNavigationDirective,
    HomeComponent,
    EligibilityUploadComponent,
    ApprovalListComponent,
    FarmLicenseExpiryComponent,
    CancelFeedOrderComponent,
    CancelInvoiceComponent,
    GuideForServicesComponent,
    FarmComponent,
    EstablishmentTypeComponent,
    AreaComponent,
    RegionComponent,
    FeedItemComponent,
    FeedItemCategoryComponent,
    ItemBusinessActivityComponent,
    SupplierComponent,
    BreedComponent,
    VaccinationProgrammeComponent,
    InspectionStatusComponent,
    DayOldChickStoreComponent,
    HatchingEggSourceComponent,
    SlaughterHouseComponent,
    UserCreationComponent,
    ApprovalSettingsComponent,
    SettingsComponent,
    FarmsReportComponent,
    EligibilityReportComponent,
    SalesReportComponent,
    SubsidyPaymentReportComponent,
    SubsidyPaymentSummaryComponent,
    InspectionReportComponent,
    ProductionCycleReportComponent,
    FulfilmentReportComponent,
    ProductionClosureReportComponent,
    FeedOrderEnquiryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
