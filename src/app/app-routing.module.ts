import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UnderConstructionComponent } from './components/under-construction/under-construction.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { DashboardLayoutComponent } from './layouts/dashboard-layout/dashboard-layout.component';
import { ApprovalListComponent } from './views/approval-list/approval-list.component';
import { CancelFeedOrderComponent } from './views/cancel-feed-order/cancel-feed-order.component';
import { CancelInvoiceComponent } from './views/cancel-invoice/cancel-invoice.component';
import { EligibilityUploadComponent } from './views/eligibility-upload/eligibility-upload.component';
import { FarmLicenseExpiryComponent } from './views/farm-license-expiry/farm-license-expiry.component';
import { AreaComponent } from './views/farmsMaster/area/area.component';
import { EstablishmentTypeComponent } from './views/farmsMaster/establishment-type/establishment-type.component';
import { FarmComponent } from './views/farmsMaster/farm/farm.component';
import { RegionComponent } from './views/farmsMaster/region/region.component';
import { FeedItemCategoryComponent } from './views/feedItemsMaster/feed-item-category/feed-item-category.component';
import { FeedItemComponent } from './views/feedItemsMaster/feed-item/feed-item.component';
import { ItemBusinessActivityComponent } from './views/feedItemsMaster/item-business-activity/item-business-activity.component';
import { GuideForServicesComponent } from './views/guide-for-services/guide-for-services.component';
import { HomeComponent } from './views/home/home.component';
import { BreedComponent } from './views/others/breed/breed.component';
import { DayOldChickStoreComponent } from './views/others/day-old-chick-store/day-old-chick-store.component';
import { HatchingEggSourceComponent } from './views/others/hatching-egg-source/hatching-egg-source.component';
import { InspectionStatusComponent } from './views/others/inspection-status/inspection-status.component';
import { SlaughterHouseComponent } from './views/others/slaughter-house/slaughter-house.component';
import { SupplierComponent } from './views/others/supplier/supplier.component';
import { VaccinationProgrammeComponent } from './views/others/vaccination-programme/vaccination-programme.component';
import { EligibilityReportComponent } from './views/reports/eligibility-report/eligibility-report.component';
import { FarmsReportComponent } from './views/reports/farms-report/farms-report.component';
import { FeedOrderEnquiryComponent } from './views/reports/feed-order-enquiry/feed-order-enquiry.component';
import { FulfilmentReportComponent } from './views/reports/fulfilment-report/fulfilment-report.component';
import { InspectionReportComponent } from './views/reports/inspection-report/inspection-report.component';
import { ProductionClosureReportComponent } from './views/reports/production-closure-report/production-closure-report.component';
import { ProductionCycleReportComponent } from './views/reports/production-cycle-report/production-cycle-report.component';
import { SalesReportComponent } from './views/reports/sales-report/sales-report.component';
import { SubsidyPaymentReportComponent } from './views/reports/subsidy-payment-report/subsidy-payment-report.component';
import { SubsidyPaymentSummaryComponent } from './views/reports/subsidy-payment-summary/subsidy-payment-summary.component';
import { ApprovalSettingsComponent } from './views/settings/approval-settings/approval-settings.component';
import { SettingsComponent } from './views/settings/settings/settings.component';
import { UserCreationComponent } from './views/settings/user-creation/user-creation.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardLayoutComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: 'farmsMaster', redirectTo: 'farmsMaster/farm' },
      { path: 'farmsMaster/farm', component: FarmComponent },
      {
        path: 'farmsMaster/establishmentType',
        component: EstablishmentTypeComponent,
      },
      { path: 'farmsMaster/area', component: AreaComponent },
      { path: 'farmsMaster/region', component: RegionComponent },
      { path: 'feedItemsMaster', redirectTo: 'feedItemsMaster/feedItem' },
      {
        path: 'feedItemsMaster/feedItem',
        component: FeedItemComponent,
      },
      {
        path: 'feedItemsMaster/feedItemCategory',
        component: FeedItemCategoryComponent,
      },
      {
        path: 'feedItemsMaster/itemBusinessActivity',
        component: ItemBusinessActivityComponent,
      },
      { path: 'others', redirectTo: 'others/supplier' },
      { path: 'others/supplier', component: SupplierComponent },
      { path: 'others/breed', component: BreedComponent },
      {
        path: 'others/vaccinationProgramme',
        component: VaccinationProgrammeComponent,
      },
      {
        path: 'others/inspectionStatus',
        component: InspectionStatusComponent,
      },
      {
        path: 'others/dayOldChicksStore',
        component: DayOldChickStoreComponent,
      },
      {
        path: 'others/hatchingEggsSource',
        component: HatchingEggSourceComponent,
      },
      { path: 'others/slaugherHouse', component: SlaughterHouseComponent },
      { path: 'settings', redirectTo: 'settings/userCreation' },
      { path: 'settings/userCreation', component: UserCreationComponent },
      {
        path: 'settings/approvalSettings',
        component: ApprovalSettingsComponent,
      },
      { path: 'settings/settings', component: SettingsComponent },
      { path: 'eligibilityUpload', component: EligibilityUploadComponent },
      { path: 'approvalList', component: ApprovalListComponent },
      { path: 'reports', redirectTo: 'reports/farmsReport' },
      { path: 'reports/farmsReport', component: FarmsReportComponent },
      {
        path: 'reports/eligibilityReport',
        component: EligibilityReportComponent,
      },
      { path: 'reports/salesReport', component: SalesReportComponent },
      {
        path: 'reports/subsidyPaymentReport',
        component: SubsidyPaymentReportComponent,
      },
      {
        path: 'reports/subsidyPaymentSummary',
        component: SubsidyPaymentSummaryComponent,
      },
      {
        path: 'reports/inspectionReport',
        component: InspectionReportComponent,
      },
      {
        path: 'reports/productionCylceReport',
        component: ProductionCycleReportComponent,
      },
      {
        path: 'reports/fulfilmentReport',
        component: FulfilmentReportComponent,
      },
      {
        path: 'reports/productionClosureReport',
        component: ProductionClosureReportComponent,
      },
      {
        path: 'reports/feedOrderEnquiry',
        component: FeedOrderEnquiryComponent,
      },
      { path: 'farmLicenseExpiry', component: FarmLicenseExpiryComponent },
      { path: 'cancelFeedOrder', component: CancelFeedOrderComponent },
      { path: 'cancelInvoice', component: CancelInvoiceComponent },
      { path: 'guideForServices', component: GuideForServicesComponent },
    ],
  },
  { path: 'auth', component: AuthLayoutComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
