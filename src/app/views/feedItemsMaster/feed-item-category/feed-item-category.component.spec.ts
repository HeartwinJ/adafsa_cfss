import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeedItemCategoryComponent } from './feed-item-category.component';

describe('FeedItemCategoryComponent', () => {
  let component: FeedItemCategoryComponent;
  let fixture: ComponentFixture<FeedItemCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FeedItemCategoryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FeedItemCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
