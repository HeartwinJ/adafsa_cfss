import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelFeedOrderComponent } from './cancel-feed-order.component';

describe('CancelFeedOrderComponent', () => {
  let component: CancelFeedOrderComponent;
  let fixture: ComponentFixture<CancelFeedOrderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CancelFeedOrderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CancelFeedOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
