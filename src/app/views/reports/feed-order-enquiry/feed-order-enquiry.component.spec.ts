import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeedOrderEnquiryComponent } from './feed-order-enquiry.component';

describe('FeedOrderEnquiryComponent', () => {
  let component: FeedOrderEnquiryComponent;
  let fixture: ComponentFixture<FeedOrderEnquiryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FeedOrderEnquiryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FeedOrderEnquiryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
