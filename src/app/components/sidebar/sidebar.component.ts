import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  items = [
    {
      label: 'Home',
      path: '/',
    },
    {
      label: 'Farms Master',
      path: '/farmsMaster',
      isOpen: false,
      subItems: [
        {
          label: 'Farm',
          path: '/farmsMaster/farm',
        },
        {
          label: 'Establishment Type',
          path: '/farmsMaster/establishmentType',
        },
        {
          label: 'Area',
          path: '/farmsMaster/area',
        },
        {
          label: 'Region',
          path: '/farmsMaster/region',
        },
      ],
    },
    {
      label: 'Feed Items Master',
      path: '/feedItemsMaster',
      isOpen: false,
      subItems: [
        {
          label: 'Feed Item',
          path: '/feedItemsMaster/feedItem',
        },
        {
          label: 'Feed Item Category',
          path: '/feedItemsMaster/feedItemCategory',
        },
        {
          label: 'Item Business Activity',
          path: '/feedItemsMaster/itemBusinessActivity',
        },
      ],
    },
    {
      label: 'Others',
      path: '/others',
      isOpen: false,
      subItems: [
        {
          label: 'Supplier',
          path: '/others/supplier',
        },
        {
          label: 'Breed',
          path: '/others/breed',
        },
        {
          label: 'Vaccination Programme',
          path: '/others/vaccinationProgramme',
        },
        {
          label: 'Inspection Status',
          path: '/others/inspectionStatus',
        },
        {
          label: 'Day-Old Chicks Source',
          path: '/others/dayOldChicksStore',
        },
        {
          label: 'Hatching Eggs Source',
          path: '/others/hatchingEggsSource',
        },
        {
          label: 'Slaughter House',
          path: '/others/slaugherHouse',
        },
      ],
    },
    {
      label: 'Settings',
      path: '/settings',
      isOpen: false,
      subItems: [
        {
          label: 'User Creation',
          path: '/settings/userCreation',
        },
        {
          label: 'Approval Settings',
          path: '/settings/approvalSettings',
        },
        {
          label: 'Settings',
          path: '/settings/settings',
        },
      ],
    },
    {
      label: 'Eligibility Upload',
      path: '/eligibilityUpload',
    },
    {
      label: 'Approval List',
      path: '/approvalList',
    },
    {
      label: 'Reports',
      path: '/reports',
      isOpen: false,
      subItems: [
        {
          label: 'Farms Report',
          path: '/reports/farmsReport',
        },
        {
          label: 'Eligibility Report',
          path: '/reports/eligibilityReport',
        },
        {
          label: 'Sales Report',
          path: '/reports/salesReport',
        },
        {
          label: 'Subsidy Payment Report',
          path: '/reports/subsidyPaymentReport',
        },
        {
          label: 'Subsidy Payment Summary',
          path: '/reports/subsidyPaymentSummary',
        },
        {
          label: 'Inspection Report',
          path: '/reports/inspectionReport',
        },
        {
          label: 'Production Cycle Report',
          path: '/reports/productionCylceReport',
        },
        {
          label: 'Fulfilment Report',
          path: '/reports/fulfilmentReport',
        },
        {
          label: 'Production Closure Report',
          path: '/reports/productionClosureReport',
        },
        {
          label: 'Feed Order Enquiry',
          path: '/reports/feedOrderEnquiry',
        },
      ],
    },
    {
      label: 'Farm License Expiry',
      path: '/farmLicenseExpiry',
    },
    {
      label: 'Cancel Feed Order',
      path: '/cancelFeedOrder',
    },
    {
      label: 'Cancel Invoice',
      path: '/cancelInvoice',
    },
    {
      label: 'Guide for the Services',
      path: '/guideForServices',
    },
  ];

  constructor(private router: Router) {}

  ngOnInit(): void {}

  toggleMenu(itemIndex: number) {
    this.items[itemIndex].isOpen = !this.items[itemIndex].isOpen;
  }
}
