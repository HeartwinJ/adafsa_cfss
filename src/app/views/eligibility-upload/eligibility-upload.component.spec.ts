import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EligibilityUploadComponent } from './eligibility-upload.component';

describe('EligibilityUploadComponent', () => {
  let component: EligibilityUploadComponent;
  let fixture: ComponentFixture<EligibilityUploadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EligibilityUploadComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EligibilityUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
