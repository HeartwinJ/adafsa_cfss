import {
  Directive,
  ElementRef,
  Input,
  OnDestroy,
  Renderer2,
} from '@angular/core';

@Directive({
  selector: '[skipNavigation][routerLink]',
})
export class SkipNavigationDirective implements OnDestroy {
  @Input() public skipNavigation = false;

  private unsubscribe: () => void;

  constructor(private ref: ElementRef, private renderer: Renderer2) {
    this.unsubscribe = renderer.listen(
      ref.nativeElement as HTMLElement,
      'click',
      (event: MouseEvent) => {
        if (this.skipNavigation) {
          event.stopImmediatePropagation();
        }
      }
    );
  }

  public ngOnDestroy(): void {
    this.unsubscribe();
  }
}
