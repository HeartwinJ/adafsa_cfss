import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DayOldChickStoreComponent } from './day-old-chick-store.component';

describe('DayOldChickStoreComponent', () => {
  let component: DayOldChickStoreComponent;
  let fixture: ComponentFixture<DayOldChickStoreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DayOldChickStoreComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DayOldChickStoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
