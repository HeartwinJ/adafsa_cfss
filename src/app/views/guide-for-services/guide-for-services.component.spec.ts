import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GuideForServicesComponent } from './guide-for-services.component';

describe('GuideForServicesComponent', () => {
  let component: GuideForServicesComponent;
  let fixture: ComponentFixture<GuideForServicesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GuideForServicesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GuideForServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
