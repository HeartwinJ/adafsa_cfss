import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmLicenseExpiryComponent } from './farm-license-expiry.component';

describe('FarmLicenseExpiryComponent', () => {
  let component: FarmLicenseExpiryComponent;
  let fixture: ComponentFixture<FarmLicenseExpiryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FarmLicenseExpiryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FarmLicenseExpiryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
