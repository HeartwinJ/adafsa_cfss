import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FarmsReportComponent } from './farms-report.component';

describe('FarmsReportComponent', () => {
  let component: FarmsReportComponent;
  let fixture: ComponentFixture<FarmsReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FarmsReportComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FarmsReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
