import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VaccinationProgrammeComponent } from './vaccination-programme.component';

describe('VaccinationProgrammeComponent', () => {
  let component: VaccinationProgrammeComponent;
  let fixture: ComponentFixture<VaccinationProgrammeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VaccinationProgrammeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VaccinationProgrammeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
