import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubsidyPaymentReportComponent } from './subsidy-payment-report.component';

describe('SubsidyPaymentReportComponent', () => {
  let component: SubsidyPaymentReportComponent;
  let fixture: ComponentFixture<SubsidyPaymentReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubsidyPaymentReportComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SubsidyPaymentReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
