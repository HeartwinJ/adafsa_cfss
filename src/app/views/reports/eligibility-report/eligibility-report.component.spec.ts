import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EligibilityReportComponent } from './eligibility-report.component';

describe('EligibilityReportComponent', () => {
  let component: EligibilityReportComponent;
  let fixture: ComponentFixture<EligibilityReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EligibilityReportComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EligibilityReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
