import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubsidyPaymentSummaryComponent } from './subsidy-payment-summary.component';

describe('SubsidyPaymentSummaryComponent', () => {
  let component: SubsidyPaymentSummaryComponent;
  let fixture: ComponentFixture<SubsidyPaymentSummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubsidyPaymentSummaryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SubsidyPaymentSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
