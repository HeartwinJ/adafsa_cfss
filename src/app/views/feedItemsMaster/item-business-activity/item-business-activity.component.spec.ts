import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemBusinessActivityComponent } from './item-business-activity.component';

describe('ItemBusinessActivityComponent', () => {
  let component: ItemBusinessActivityComponent;
  let fixture: ComponentFixture<ItemBusinessActivityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemBusinessActivityComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ItemBusinessActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
