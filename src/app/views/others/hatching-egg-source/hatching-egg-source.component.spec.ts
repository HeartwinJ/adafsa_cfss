import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HatchingEggSourceComponent } from './hatching-egg-source.component';

describe('HatchingEggSourceComponent', () => {
  let component: HatchingEggSourceComponent;
  let fixture: ComponentFixture<HatchingEggSourceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HatchingEggSourceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HatchingEggSourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
