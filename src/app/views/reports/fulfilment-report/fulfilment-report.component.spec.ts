import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FulfilmentReportComponent } from './fulfilment-report.component';

describe('FulfilmentReportComponent', () => {
  let component: FulfilmentReportComponent;
  let fixture: ComponentFixture<FulfilmentReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FulfilmentReportComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FulfilmentReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
