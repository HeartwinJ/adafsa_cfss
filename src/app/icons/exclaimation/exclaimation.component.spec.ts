import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExclaimationComponent } from './exclaimation.component';

describe('ExclaimationComponent', () => {
  let component: ExclaimationComponent;
  let fixture: ComponentFixture<ExclaimationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExclaimationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExclaimationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
